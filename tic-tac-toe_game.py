import sys
import pygame


def check_win(arr, sign):
    """The function checks array in a block with signs-"X" or "O"
     and returns a sign of winner"""
    zeroes = 0
    for i in range(3):
        zeroes += arr[i].count(0)
        if arr[i][0] == arr[i][1] == arr[i][2] == sign:
            return sign
        if arr[0][i] == arr[1][i] == arr[2][i] == sign:
            return sign
        if arr[0][0] == arr[1][1] == arr[2][2] == sign:
            return sign
        if arr[0][2] == arr[1][1] == arr[2][0] == sign:
            return sign
    if zeroes == 0:
        return "Piece"
    return False


pygame.init()

# creating of sizes screen and three-by-three grid
size_block = 100  # size of screen
margin = 15  # margin between blocks
width = heigh = size_block*3 + margin*4
size_window = (width, heigh)
img = pygame.image.load("img/tic-tac-toe.png")
pygame.display.set_icon(img)

screen = pygame.display.set_mode(size_window)
pygame.display.set_caption("TIC - TAC - TOE")  # title of screen

# Colors of blocks
black = (0, 0, 0)
white = (250, 243, 221)
grey = (105, 109, 125)
light_blue = (104, 176, 171)

# creating an array
arr = [[0]*3 for i in range(3)]
query = 0
game_over = False


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: # for exit game
            pygame.quit()
            sys.exit(0)
        elif event.type == pygame.MOUSEBUTTONDOWN and not game_over:
            x_mouse, y_mouse = pygame.mouse.get_pos()
            col = x_mouse//(margin+size_block)
            row = y_mouse//(margin+size_block)
            if arr[row][col] == 0: # creating turns
                if query % 2 == 0:
                    arr[row][col] = "X"
                else:
                    arr[row][col] = "O"
                query += 1
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:  # starting over by space bar
            screen.fill(black)
            game_over = False
            arr = [[0] * 3 for i in range(3)]
            query = 0

    if not game_over:
        for row in range(3):
            for col in range(3):
                if arr[row][col] == "X":
                    color = light_blue
                elif arr[row][col] == "O":
                    color = grey
                else:
                    color = white
                x = col * size_block + margin*(col+1)
                y = row * size_block + (row+1)*margin
                pygame.draw.rect(screen, color, (x, y, size_block, size_block))
                if color == light_blue:
                    pygame.draw.line(screen, white, (x+20, y+20), (x+size_block-20, y+size_block-20), 10)
                    pygame.draw.line(screen, white, (x+size_block-20, y + 20), (x+20, y + size_block - 20), 10)
                elif color == grey:
                    pygame.draw.circle(screen, white, (x+size_block//2, y+size_block//2), size_block//2-12,8)
    if (query-1)%2 == 0:
        game_over = check_win(arr, "X")
    else:
        game_over = check_win(arr, "O")

    if game_over:
        screen.fill(black)
        font = pygame.font.SysFont('stxingkai', 80)
        text_1 = font.render("Winner: "+game_over, True, white)
        text_rect = text_1.get_rect()
        text_x = screen.get_width()/2 - text_rect.width/2
        text_y = screen.get_height()/2 - text_rect.height/2
        screen.blit(text_1, [text_x, text_y])

    pygame.display.update()
